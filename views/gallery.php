<?php  

// in order for us to access the stored data in
// the $_SESSION across different pages, we
// just initialize the session with the session
// start function
// syntax: session_start()

// once we hae initialized the session. we can
// then freely access all the session variables
// and its values stored in the current
// $_SESSION and use it as we please

// if($_SESSION['email'] == "admin@email.com" ) {
	// echo "Hello admin";
// }

	require_once '../partials/header.php';

	function getTitle() {
		return 'Gallery Page';
	}

?>



	<div class="container">
		<h2 class="text-center">Products Dashboard</h2>
		<div class="row">
		<?php  
			// retrieve all the products in products.json as a string
			$products = file_get_contents('../assets/lib/products.json');
			// var_dump($products);

			// convert to an assoc array
			$products_array = json_decode($products, true);

			// var_dump the array
			// var_dump($products_array);

			// iterate the array
			foreach($products_array as $product){
				// var_dump($product);
		?>
			<div class="col-md-4">
				<div class="card">
					<img src="<?= $product['image'];  ?>" class="card-img-top">
					<div class="card-body">
						<h5 class="card-title"><?= $product['name'];  ?></h5>
						<p class="card-text">Price: <?= $product['price'];  ?></p>
						<p class="card-text">Description: <?= $product['description'];  ?></p>
					</div> <!-- end card body -->
				</div> <!-- end card -->
			</div> <!-- end cols -->
		<?php  } ?>
		</div> <!-- end row -->

	</div>




<?php  
	require_once '../partials/footer.php';
?>