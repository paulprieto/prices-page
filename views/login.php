<?php  
	
	require_once '../partials/header.php';

	function getTitle() {
		return "Login Page";
	}

?>

	<div class="container-fluid">
		<h2 class="text-center"> Login Page </h2>

		<div class="row">
			<div class="col-md-8 mx-auto">
				<form action="../controllers/authenticate.php" method="POST">
					<div class="form-group">
						<label for="email"> Email </label>
						<input type="email" id="email" name="email" class="form-control">
					</div>

					<div class="form-group">
						<label for="password"> Password </label>
						<input type="password" id="password" name="password" class="form-control">
					</div>

					<button type="submit" class="btn btn-primary btn-block"> Login </button>
				</form> <!-- end form -->
			</div> <!-- end cols -->
		</div> <!-- end row -->
	</div> <!-- end container -->






<?php require_once '../partials/footer.php'; ?>