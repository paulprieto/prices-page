<?php
require_once '../partials/header.php';

function getTitle()
{
  return "Register Page";
}
?>

<?php
$prices = file_get_contents('../assets/lib/prices.json');

$prices_array = json_decode($prices, true);
?>

<div class="container-fluid pricing__container">
  <div class="container">
    <div class="pricing__title_container">
      Package <span class="font-weight-bolder">Pricing</span>
    </div>

    <div class="pricing__box">
      <div class="container">
        <div class="row first_box">
          <p> Be Part of the 100% of Students</p>
          <p> Who Raise Their MCAT Score an Average of 12 Points!</p>
        </div>

        <div class="row second_box">
          <div class="container">
            <div class="row benefits_text">
              Benefits of Every Package:
            </div>
            <div class="row benefits_container">
              <div class="col-12 col-sm-12 col-lg-4 text-center benefits_item"><img class="benefits_item_image" src="../assets/images/camel.png" alt=""> <a href=""> Exclusive Content <sup><i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Exclusive Tooltip"></i></sup></a></div>
              <div class="col-12 col-sm-12 col-lg-4 text-center benefits_item"><img class="benefits_item_image" src="../assets/images/camel.png" alt=""><a href=""> Study Smart, Not Harder <sup><i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Study Smart Tooltip"></i></sup></a>
              </div>
              <div class="col-12 col-sm-12 col-lg-4 text-center benefits_item"><img class="benefits_item_image" src="../assets/images/camel.png" alt=""><a href=""> Personalized Study Plan <sup><i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Personalized study plan Tooltip"></i><sup></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="pricing_table_container">
      <p>Choose your tutor:</p>

      <ul class="nav nav-pills mb-3 pricing_tabs" id="pills-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link" id="pills-standard-tab" data-toggle="pill" href="#pills-standard" role="tab" aria-controls="pills-standard" aria-selected="false">Standard <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Standard Tooltip"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" id="pills-master-tab" data-toggle="pill" href="#pills-master" role="tab" aria-controls="pills-master" aria-selected="true">Master <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Master Tooltip"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="pills-director-tab" data-toggle="pill" href="#pills-director" role="tab" aria-controls="pills-director" aria-selected="false">Director <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Director Tooltip"></i></a>
        </li>
      </ul>
    </div>

    <div class="tab-content pricing_tab_content_container" id="pills-tabContent">
      <div class="tab-pane fade" id="pills-standard" role="tabpanel" aria-labelledby="pills-standard-tab">

        <div class="row">
          <?php foreach ($prices_array as $price) { ?>
            <div class="pricing_tab_content">
              <img class="img-fluid" src="../assets/images/camel.png" alt="">

              <div class="container pricing_tab_item_container">
                <div class="row pricing_tab_item_title">
                  <?= $price['name'];  ?>
                </div>

                <div class="row hours_container">
                  <div class="col hours">
                    <?= $price['hours_included'];  ?>
                  </div>
                  <div class="col hours_description">
                    One-on-One Private Tutoring Hours
                    <!-- <div class="fluid-container ">
                    <div class="row"></div>
                    <div class="row">Tutoring Hours</div>
                  </div> -->
                  </div>
                </div>

                <div class="row price">
                  $<?= $price['price_per_hour'];  ?>
                </div>

                <div class="row price_switch_container">
                  <!-- php function add 'highlight' class if disabled-->
                  <label class="left_label" for="customSwitch1">total</label>
                  <div class="custom-control custom-switch price_switch">
                    <input type="checkbox" class="custom-control-input" id="customSwitch1">
                    <!-- php function add 'highlight' class if enabled-->
                    <label class="custom-control-label" for="customSwitch1">hourly rate</label>
                  </div>
                </div>

                <div class="row price_label">
                  Starting at <span> $<?= $price['price_per_month'];  ?></span>/mo
                </div>
                <div class="row price_label">
                  with <span> </span> <span class="text-underline">affirm</span>
                </div>

                <div class="row price_signup_button_container">
                  <button class="btn btn-block btn-success">Sign Up</button>
                </div>


                <button class="btn showdetails_button hidden desktop_hidden collapse_block_button" type="button" data-toggle="collapse" data-target="#pricing_benefits" aria-expanded="false" aria-controls="pricing_benefits">
                  Show Details
                  <!-- if showdetails == false -->
                  <div><i class="fas fa-chevron-down"></i></div>

                  <!-- if showdetails == false -->
                  <!-- <div><i class="fas fa-chevron-up"></i></div> -->
                </button>

                <div class="pricing_benefits_container">
                  <div class="collapse dont-collapse-sm" id="pricing_benefits">
                    <div class="container-fluid benefits_list_container">

                      <?php foreach ($price['benefits'] as $benefit) { ?>
                        <div class="row benefits_list_item">
                          <div class="col-3">
                            <img class="benefits_item_image" src="../assets/images/camel.png" alt="">
                          </div>
                          <div class="col-9 benefits_list_item_description">
                            <?= $benefit;  ?>
                          </div>
                        </div>
                      <?php  } ?>

                    </div>
                  </div>
                </div>

              </div>
              <!-- end of tab_item_container -->
              <?php if ($price['additional_benefits']) { ?>
                <div class="container-fluid additional_benefits_list_container collapse dont-collapse-sm" id="pricing_benefits">

                  <div class="container">

                    <div class="additioal_benefits_text">Plus, you'll also receive:</div>

                    <?php foreach ($price['additional_benefits'] as $additional_benefit) { ?>
                      <div class="row benefits_list_item">
                        <div class="col-3">
                          <img class="benefits_item_image" src="../assets/images/camel.png" alt="">
                        </div>
                        <div class="col-9 benefits_list_item_description">
                          <?= $additional_benefit;  ?>
                        </div>
                      </div>
                    <?php  } ?>

                    <!-- <div class="row benefits_list_item">
                    <div class="col-3">
                      <img class="benefits_item_image" src="../assets/images/camel.png" alt="">
                    </div>
                    <div class="col-9 benefits_list_item_description">
                      Dedicated Student Success Manager
                    </div>
                  </div>

                  <div class="row benefits_list_item">
                    <div class="col-3">
                      <img class="benefits_item_image" src="../assets/images/camel.png" alt="">
                    </div>
                    <div class="col-9 benefits_list_item_description">
                      Dedicated Student Success Manager
                    </div>
                  </div> -->
                  </div>
                </div>
              <?php  } ?>
            </div>
          <?php  } ?>
        </div>

      </div>
      <div class="tab-pane fade show active" id="pills-master" role="tabpanel" aria-labelledby="pills-master-tab">Master
      </div>
      <div class="tab-pane fade" id="pills-director" role="tabpanel" aria-labelledby="pills-director-tab">Director</div>
    </div>

  </div>
</div>

<?php
require "../partials/footer.php";
?>