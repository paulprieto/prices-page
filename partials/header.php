<?php
session_start();
// var_dump($_SESSION);
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">

  <title> MedSchoolCoach | <?php echo getTitle(); ?> </title>

  <!-- animate css -->


  <!-- fontawesome icons -->
  <script src="https://kit.fontawesome.com/6f699f8432.js" crossorigin="anonymous"></script>
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"> -->

  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

  <!-- external css -->
  <link rel="stylesheet" href="../assets/css/style.css">

  <!-- <meta http-equiv="refresh" content="1" >  -->


</head>

<body>